-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Sep 2019 pada 06.37
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_voteplatform`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `business_ideas`
--

CREATE TABLE `business_ideas` (
  `business_idea_id` int(11) NOT NULL,
  `business_idea` varchar(256) NOT NULL,
  `business_desc` longtext NOT NULL,
  `business_logo` varchar(256) NOT NULL,
  `competition_day` int(11) NOT NULL,
  `competition_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `business_ideas`
--

INSERT INTO `business_ideas` (`business_idea_id`, `business_idea`, `business_desc`, `business_logo`, `competition_day`, `competition_status`) VALUES
(1, 'Arisan Online', 'Arisan secara online', 'templatelogo.png', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `business_idea_votes`
--

CREATE TABLE `business_idea_votes` (
  `vote_id` int(11) NOT NULL,
  `business_idea_id` int(11) NOT NULL,
  `voted_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `startups`
--

CREATE TABLE `startups` (
  `startup_id` int(11) NOT NULL,
  `startup_name` varchar(64) NOT NULL,
  `startup_desc` text NOT NULL,
  `startup_logo` varchar(256) NOT NULL,
  `startup_email` varchar(64) NOT NULL,
  `startup_password` varchar(255) NOT NULL,
  `startup_vote_remains` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `startups`
--

INSERT INTO `startups` (`startup_id`, `startup_name`, `startup_desc`, `startup_logo`, `startup_email`, `startup_password`, `startup_vote_remains`) VALUES
(1, 'Vallet', 'Desain grafis online pertama di Indonesia', '', 'office@vallet.id', '$2y$10$CVLbdS2AjzKbB6iKpvitM.5TwPKpnsqPokAlQrdqh72LxHxarD0Te', 0),
(2, 'FlipApp', 'Platform jual-beli aplikasi', '', 'office@flipapp.id', '$2y$10$CVLbdS2AjzKbB6iKpvitM.5TwPKpnsqPokAlQrdqh72LxHxarD0Te', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `startup_votes`
--

CREATE TABLE `startup_votes` (
  `vote_id` int(11) NOT NULL,
  `startup_id` int(11) NOT NULL,
  `voted_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `startup_votes`
--

INSERT INTO `startup_votes` (`vote_id`, `startup_id`, `voted_by`) VALUES
(1, 2, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(64) NOT NULL,
  `user_email` varchar(64) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_vote_remains` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `business_ideas`
--
ALTER TABLE `business_ideas`
  ADD PRIMARY KEY (`business_idea_id`);

--
-- Indeks untuk tabel `business_idea_votes`
--
ALTER TABLE `business_idea_votes`
  ADD PRIMARY KEY (`vote_id`),
  ADD KEY `user_id` (`business_idea_id`),
  ADD KEY `user_liked_by` (`voted_by`);

--
-- Indeks untuk tabel `startups`
--
ALTER TABLE `startups`
  ADD PRIMARY KEY (`startup_id`);

--
-- Indeks untuk tabel `startup_votes`
--
ALTER TABLE `startup_votes`
  ADD PRIMARY KEY (`vote_id`),
  ADD KEY `user_id` (`startup_id`),
  ADD KEY `user_liked_by` (`voted_by`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `business_ideas`
--
ALTER TABLE `business_ideas`
  MODIFY `business_idea_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `business_idea_votes`
--
ALTER TABLE `business_idea_votes`
  MODIFY `vote_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `startups`
--
ALTER TABLE `startups`
  MODIFY `startup_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `startup_votes`
--
ALTER TABLE `startup_votes`
  MODIFY `vote_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `business_idea_votes`
--
ALTER TABLE `business_idea_votes`
  ADD CONSTRAINT `business_idea_votes_ibfk_1` FOREIGN KEY (`business_idea_id`) REFERENCES `business_ideas` (`business_idea_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `business_idea_votes_ibfk_2` FOREIGN KEY (`voted_by`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `startup_votes`
--
ALTER TABLE `startup_votes`
  ADD CONSTRAINT `startup_votes_ibfk_1` FOREIGN KEY (`startup_id`) REFERENCES `startups` (`startup_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `startup_votes_ibfk_2` FOREIGN KEY (`voted_by`) REFERENCES `startups` (`startup_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
