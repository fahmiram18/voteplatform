<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        is_logged_in_business();
        $this->load->model('business_model', 'vote');
        $this->load->model('user_model', 'user');
    }
    
    public function index()
    {
        was_voted_business();
        $username = $this->session->userdata("user_name");
        $data = [
            'title' => 'VotePlatform',
            'business' => $this->vote->get(),
            'stats' => $this->vote->stats()
        ];
        $this->load->view('layouts/header2', $data);
        $this->load->view('vote/index', $data);
        $this->load->view('layouts/footer2');
    }

    public function give($business_idea_id)
    {
        sleep(1);
        $user_id = $this->session->userdata('user_id');
        $user = $this->user->get($user_id);
        if ($user['user_vote_remains'] == 1) {
            $data = [
                'business_idea_id' => $business_idea_id,
                'voted_by' => $user_id
            ];
            $vote = $this->vote->vote($data);
            redirect('business/stats');
        } else {
            redirect('business/stats');
        }
    }

    public function stats()
    {
        $data = [
            'title' => 'Stats | VotePlatform',
            'stats' => $this->vote->stats()
        ];
        $this->load->view('layouts/header2', $data);
        $this->load->view('vote/stats', $data);
        $this->load->view('layouts/footer2');
    }

}

/* End of file Business.php */