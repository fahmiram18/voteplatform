<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index()
    {
        $data = [
            'title' => 'StartupChain!'
        ];
        $this->load->view('layouts/header', $data);
        $this->load->view('home/index');
        $this->load->view('layouts/footer');
    }

}

/* End of file Home.php */
