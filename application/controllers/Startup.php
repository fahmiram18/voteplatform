<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Startup extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        is_logged_in_startup();
        $this->load->model('startup_model', 'startup');
        $this->load->model('user_model', 'user');
    }
    
    public function index()
    {
        was_voted();
        $startup_email = $this->session->userdata("startup_email");
        $data = [
            'startups' => $this->startup->get($startup_email),
            'stats' => $this->startup->stats()
        ];
        $this->load->view('layouts/header');
        $this->load->view('startup/index', $data);
        $this->load->view('layouts/footer2');
    }

    public function vote()
    {
        sleep(1);
        $startup_id = $this->input->post('startup_id');
        $voted_by = $this->session->userdata('startup_id');
        $startup = $this->startup->getData($this->session->userdata("startup_email"));
        // var_dump($startup);
        // die;
        if ($startup['startup_vote_remains'] == 1) {
            $data = [
                'startup_id' => $startup_id,
                'voted_by' => $voted_by
            ];
            $vote = $this->startup->vote($data);
            redirect('startup/stats');
        } else {
            redirect('startup');
        }
    }

    public function stats()
    {
        $data = [
            'startups' => $this->startup->get($this->session->userdata("startup_email")),
            'stats' => $this->startup->stats()
        ];
        $this->load->view('layouts/header');
        $this->load->view('startup/stats', $data);
        $this->load->view('layouts/footer2');
    }

}

/* End of file Startup.php */