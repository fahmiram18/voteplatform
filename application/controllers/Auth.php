<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct() {
        parent::__construct();
        $this->load->model('auth_model', 'auth');
    }

    public function business() 
    {
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'trim|required');

        if (!$this->form_validation->run()) {
            $this->load->view('layouts/header');
            $this->load->view('auth/business');
            $this->load->view('layouts/footer');
        } else if ($this->form_validation->run()) {
            $this->_login_business();
        }
    }

    public function startup()
    {
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'trim|required');

        if ($this->form_validation->run()) {
            $this->_login_startup();
        } else {
            $this->load->view('layouts/header');
            $this->load->view('auth/startup');
            $this->load->view('layouts/footer');
        }
    }

    private function _login_startup() {

        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $startup = $this->auth->getStartup($email);
        if (password_verify($password, $startup['startup_password'])) {
            $data = [
                'startup_id' => $startup['startup_id'],
                'startup_name' => $startup['startup_name'],
                'startup_email' => $startup['startup_email'],
                'startup_vote_remains' => $startup['startup_vote_remains']
            ];

            $this->session->set_userdata($data);
            redirect('startup/stats');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Email is not Registered!
            </div>');
            redirect('auth/startup');
        }
    }

    private function _login_business()
    {

        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $user = $this->auth->getUser($email);
        if (password_verify($password, $user['user_password'])) {
            $data = [
                'user_id' => $user['user_id'],
                'user_name' => $user['user_name'],
                'user_email' => $user['user_email'],
                'user_vote_remains' => $user['user_vote_remains']
            ];
            $this->session->set_userdata($data);
            redirect('business/stats');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Email is not Registered!
            </div>');
            redirect('auth/business');
        }
    }

    public function registration() {
        $data = [
            'title' => 'Login'
        ];
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[8]');

        if (!$this->form_validation->run()) {
            $this->load->view('layouts/header', $data);
            $this->load->view('auth/registration');
            $this->load->view('layouts/footer');
        } else if ($this->form_validation->run()) {
            $this->_createAccount();
        }
    }

    private function _createAccount() {
        $data = [
            'user_name' => $this->input->post('name'),
            'user_email' => $this->input->post('email'),
            'user_password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
        ];

        if ($this->auth->getUser($data['email'])) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Email has been registered!
            </div>');
        } else {
            $account = $this->auth->create($data);

            if ($account) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    Account has been created!
                </div>');
                redirect('auth/business');
            }
        }
    }

    public function logout() {
        session_destroy();
        redirect('home','refresh');
    }
}

/* End of file Auth.php */
