<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model
{

    private $table_name = 'users';

    public function getStartup($email = null)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            startup_id,
            startup_name,
            startup_email,
            startup_password,
            startup_vote_remains
        ');
        $this->db->from('startups');
        $this->db->where('startup_email', $email); 
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            if (count($result) == 0) {
                return 0;
            } else {
                return $result[0];
            }
        } else {
            return FALSE;
        }
    }

    public function getUser($email = null)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            user_id,
            user_name,
            user_email,
            user_password,
            user_vote_remains
        ');
        $this->db->from('users');
        $this->db->where('user_email', $email);
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            if (count($result) == 0) {
                return 0;
            } else {
                return $result[0];
            }
        } else {
            return FALSE;
        }
    }

    public function create($data)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->insert($this->table_name,$data);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return FALSE;
        }
    }
}

/* End of file auth_model.php */
