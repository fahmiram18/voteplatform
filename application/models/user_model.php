<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class user_model extends CI_Model {

    private $table_name = 'users';

    public function get($user_id)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            user_id,
            user_name,
            user_vote_remains
        ');
        $this->db->from($this->table_name);
        $this->db->where('user_id', $user_id);
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            if (count($result) == 0) {
                return 0;
            } else {
                return $result[0];
            }
        } else {
            return FALSE;
        }
    }

}

/* End of file user_model.php */
