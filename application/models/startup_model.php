<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class startup_model extends CI_Model {

    public function get($startup_email) {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            startup_id,
            startup_name,
            startup_desc,
            startup_logo,
            ');
            $this->db->from('startups');
            $this->db->where('startup_email !=', $startup_email);
            $result = $this->db->get()->result_array();
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            if (count($result) > 0) {
                return $result;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
    
    public function getData($startup_email)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            startup_id,
            startup_name,
            startup_desc,
            startup_logo,
            startup_vote_remains
        ');
        $this->db->from('startups');
        $this->db->where('startup_email', $startup_email);
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            if (count($result) > 0) {
                return $result[0];
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function vote($data) {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $vote = $this->db->get_where('startup_votes', $data)->result_array();
        if (count($vote) > 0) {
            $result = false;
        } else {
            $this->db->insert('startup_votes', $data);
            $this->db->where('startup_id', $data['voted_by']);
            $this->db->update('startups', ['startup_vote_remains' => 0]);
            $result = $this->db->affected_rows();
        }
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            $this->session->set_userdata(['startup_vote_remains' => 0]);
            return $result;
        } else {
            return $result;
        }
    }

    public function stats()
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            COUNT(startup_votes.voted_by) as jumlah_suara,
            startups.startup_name as startup_terpilih
        ');
        $this->db->from('startup_votes');
        $this->db->join('startups', 'startups.startup_id=startup_votes.startup_id');
        $this->db->group_by('startups.startup_name');
        $this->db->order_by('COUNT(startup_votes.voted_by)', 'desc');
        $this->db->limit(10);
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return FALSE;
        }
    }

}

/* End of file startup_model.php */
