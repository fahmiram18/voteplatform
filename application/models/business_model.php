<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class business_model extends CI_Model {

    public function get()
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            business_idea_id,
            business_idea,
            business_desc,
            business_logo,
            competition_day,
            competition_status
            ');
        $this->db->from('business_ideas');
        $this->db->where('competition_day', 1);
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            if (count($result) > 0) {
                return $result;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function getData($startup_email)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            startup_id,
            startup_name,
            startup_desc,
            startup_logo,
            startup_vote_remains
        ');
        $this->db->from('startups');
        $this->db->where('startup_email', $startup_email);
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            if (count($result) > 0) {
                return $result[0];
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function vote($data)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $vote = $this->db->get_where('business_idea_votes', $data)->result_array();
        if (count($vote) > 0) {
            $result = false;
        } else {
            $this->db->insert('business_idea_votes', $data);
            $this->db->where('user_id', $data['voted_by']);
            $this->db->update('users', ['user_vote_remains' => 0]);
            $result = $this->db->affected_rows();
        }
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            $this->session->set_userdata(['user_vote_remains' => 0]);
            return $result;
        } else {
            return $result;
        }
    }

    public function stats()
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            COUNT(business_idea_votes.voted_by) as jumlah_suara,
            business_ideas.business_idea as business_terpilih
        ');
        $this->db->from('business_idea_votes');
        $this->db->join('business_ideas', 'business_ideas.business_idea_id=business_idea_votes.business_idea_id');
        $this->db->group_by('business_ideas.business_idea');
        $this->db->order_by('COUNT(business_idea_votes.voted_by)', 'desc');
        $this->db->limit(6);
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            return $result;
        } else {
            return FALSE;
        }
    } 

}

/* End of file business_model.php */