<?php 
    $labels = [];
    $data = [];

    foreach ($stats as $s) {
        array_push($labels, $s['startup_terpilih']);
        array_push($data, $s['jumlah_suara']);
    }

    // var_dump(json_encode($labels));
    // var_dump(json_encode($data));
?>

<body>
    <div class="container mt-3">
        <div class='row'>
            <div class="col-12">
                <canvas id="myChart"></canvas>
            </div>
        </div>
    </div>
    <script src='<?=site_url('asset/chartjs/Chart.js')?>'></script>
	<script>
		var ctx = document.getElementById("myChart").getContext('2d');
		var myChart = new Chart(ctx, {
            type: 'horizontalBar',
			data: {
                labels: <?= json_encode($labels); ?>,
				datasets: [{
                    label: 'Vote Result',
					data: <?= json_encode($data); ?>,
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true
                        },
                        // barPercentage: 0.7,
                        // barThickness: 6,
                        // maxBarThickness: 8,
					}]
				}
			}
        });
        $( document ).ready(function() {
            setTimeout(function(){
                window.location.reload(1);
            }, 5000);
        });
	</script>
</body>