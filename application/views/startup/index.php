<body>
    <div class="container mt-3">
        <h3 class="mt-2 mb-3">Vote List</h3>
        <div class="row">
            <div class="col-8">
                <?php if ($this->session->userdata('startup_id')) : ?>
                    <?php foreach($startups as $startup) : ?>
                        <div>
                            <div class="card mb-3">
                                <div style="display: flex; flex: 1 1 auto;">
                                    <div class="img-square-wrapper">
                                        <img class="" src="<?= base_url() . '/asset/img/ballot-black-and-white-black-and-white-1550337.jpg' ?>" alt="<?= $startup['startup_name'] ?>" width="200rem">
                                    </div>
                                    <div class="card-body">
                                        <h4 class="card-title"><?= $startup['startup_name'] ?></h4>
                                        <p class='card-text'><?= $startup['startup_desc'] ?></p>
                                        <div class="mt-3">
                                            <form action="<?= site_url('startup/vote')?>"  method="post">
                                                <input type="hidden" name='startup_id' value='<?= $startup['startup_id']; ?>' >
                                                <button class='btn btn-danger text-wrap text-white' style="width: 8rem;" onclick="successVote()" >Vote!</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else : ?>
                    <a href="<?php echo base_url('auth') ?>">Log In</a>
                <?php endif ?>
            </div>
            <div class="col-4">
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title text-center">Top 10 Vote Result</h5>
                        <hr>
                        <div class="row">
                            <?php $i = 1;?>
                            <?php foreach($stats as $s) : ?>
                                <div class="col-3">
                                    <p class="card-text">#<?=$i++?></p>
                                </div>
                                <div class="col-9">
                                    <p class="card-text"><?=$s['startup_terpilih']?></p>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</body>
<script>
    function successVote() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        })

        Toast.fire({
            type: 'success',
            title: 'Voted Successfully!'
        })
    }
</script>