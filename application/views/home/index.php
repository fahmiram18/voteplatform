<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title; ?></title>
    <style>
        body {
            background-image: url(<?= base_url() . "/asset/img/prism.png" ?>);
        }

        .card-img-top {
            height: 35vh;
            object-fit: cover;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row mt-5">
            <div class="col-12">
                <h1 class='display-3 text-center text-white'>Welcome to
                    <div class="badge badge-light text-wrap text-danger">
                        <span>StartupChain!</span>
                    </div>
                </h1>
                <hr color="white">
                <h4 class='text-center text-white font-weight-lighter'>Where you are the <b class='font-weight-bold'>Judge!</b></h4>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col mb-3">
                <div class="card">
                    <img src="<?= base_url() ?>/asset/img/close-up-hand-ideas-1811996.jpg" class="card-img-top" alt="Business Idea Competition" width="50px">
                    <div class="card-body">
                        <h4 class="card-title text-center">Business Idea Competition</h4>
                        <p class="card-text text-center">Go vote your favourite Business Idea!</p>
                        <a href="<?= base_url('business') ?>" class="btn btn-block btn-danger">Vote Here!</a>
                    </div>
                </div>
            </div>
            <div class="col mb-3">
                <div class="card">
                    <img src="<?= base_url() ?>/asset/img/adults-analysis-brainstorming-1661004.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h4 class="card-title text-center">Startup Competition</h4>
                        <p class="card-text text-center">Go vote your favourite Startup!</p>
                        <a href="<?= base_url('startup') ?>" class="btn btn-block btn-danger">Vote Here!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>