<!DOCTYPE html>
<html lang="en">

<head>

</head>
<style>
    body {
        background-image: url(<?= base_url() . "/asset/img/prism.png" ?>);
    }
</style>

<body>
    <div class="container">
        <div class="row mt-5">
            <div class="col-12">
                <h1 class='display-3 text-center text-white'>Welcome to
                    <div class="badge badge-light text-wrap text-danger">
                        <span>StartupChain!</span>
                    </div>
                </h1>
                <hr color="white">
                <h4 class='text-center text-white font-weight-lighter'>Where you are the <b class='font-weight-bold'>Judge</b></h4>
            </div>
        </div>
        <div class="row mt-3 justify-content-center">
            <div class="col-md-6 col-sm-12">
                <form action="<?= site_url('auth/business') ?>" method='POST'>
                    <div class="form-group text-center">
                        <label for="email" class="text-white">E-mail</label>
                        <input type="text" class="form-control" id='email' name='email' placeholder='Enter email' value='<?php echo set_value('email') ?>'>
                    </div>
                    <div class="form-group text-center">
                        <label for="password" class="text-white">Password</label>
                        <input type="password" class="form-control" id='password' placeholder='Enter password' name='password' value='<?php echo set_value('password') ?>'><br>
                    </div>
                    <button class='btn btn-danger btn-block mt-n3 mb-3' type='submit'>Login</button>
                </form>
                <div class='text-center text-white font-weight-lighter mb-5'>
                    Don't have an account? <a href="<?= site_url('auth/registration') ?>"><b class='font-weight-bold text-danger'>Register Here!</b></a>
                    <b class='font-weight-bold'><?= form_error('email'); ?></b>
                    <b class='font-weight-bold'><?= form_error('password'); ?></b>
                </div>
            </div>
        </div>
    </div>
</body>

</html>