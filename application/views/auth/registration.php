<!DOCTYPE html>
<html lang="en">

<head>

</head>
<style>
    body {
        background-image: url(<?= base_url() . "/asset/img/prism.png" ?>);
    }
</style>

<body>
    <div class="container">
        <div class="row mt-5">
            <div class="col-12">
                <h1 class='display-3 text-center text-white'>Welcome to
                <img src="<?= base_url() . "/asset/img/logo.png" ?>" alt="" class="img-fluid">
                </h1>
                <hr color="white">
                <h4 class='text-center text-white font-weight-lighter'>Where you are the <b class='font-weight-bold'>Judge</b></h4>
            </div>
        </div>
        <div class="row mt-3 justify-content-center">
            <div class="col-md-6 col-sm-12">
                <form action="<?= site_url('auth/registration') ?>" method='POST'>
                    <div class="form-group text-center">
                        <label for="name" class="text-white">Fullname</label>
                        <input type="text" placeholder='Enter your name' class="form-control" id='name' name='name' value='<?php echo set_value('name') ?>'>
                    </div>
                    <div class="form-group text-center">
                        <label for="email" class="text-white">E-mail</label>
                        <input type="text" class="form-control" id='email' name='email' placeholder='Enter your email' value='<?php echo set_value('email') ?>'>
                    </div>
                    <div class="form-group text-center">
                        <label for="password" class="text-white">Password</label>
                        <input type="password" placeholder='Enter your password' class="form-control" id='password' name='password' value='<?php echo set_value('password') ?>'><br>
                    </div>
                    <button class='btn btn-danger btn-block mt-n3 mb-3' type='submit'>Register</button>
                </form>
                <div class='text-center text-white font-weight-lighter mb-5'>
                    Already have an account?
                    <a href="<?= site_url('auth') ?>"><b class='font-weight-bold text-danger'>Login Here!</b></a>
                    <?= form_error('name'); ?>
                    <?= form_error('email'); ?>
                    <?= form_error('password'); ?>
                </div>
            </div>
        </div>
    </div>
</body>

</html>