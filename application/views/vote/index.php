<body>
    <div class="container mt-3">
        <h3 class="mt-2 mb-3">Vote List</h3>
        <div class="row">
            <div class="col-8">
                <?php if ($this->session->userdata('user_id')) : ?>
                    <?php foreach ($business as $a) : ?>
                        <div>
                            <div class="card mb-3">
                                <div style="display: flex; flex: 1 1 auto;">
                                    <div class="img-square-wrapper">
                                        <img class="" src="<?= base_url() . 'asset/img/' . $a['business_logo'] ?>" alt="<?= $a['business_idea'] ?>" width="200rem">
                                    </div>
                                    <div class="card-body">
                                        <h4 class="card-title"><?= $a['business_idea'] ?>
                                            <div class="mt-3">
                                            <a href="<?php echo base_url('business/give/') . $a['business_idea_id'] ?>" class=" badge badge-danger text-wrap text-white" style="width: 8rem;" onclick="successVote()">Vote!</a>
                                            </div>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else : ?>
                    <a href="<?php echo base_url('auth') ?>">Log In</a>
                <?php endif ?>
            </div>
            <div class="col-4">
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title text-center">Top 10 Vote Result</h5>
                        <hr>
                        <div class="row">
                            <?php $i = 1;?>
                            <?php foreach($stats as $s) : ?>
                                <div class="col-3">
                                    <p class="card-text">#<?=$i++?></p>
                                </div>
                                <div class="col-9">
                                    <p class="card-text"><?=$s['business_dipilih']?></p>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</body>
<script>
    function successVote() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        })

        Toast.fire({
            type: 'success',
            title: 'Voted Successfully!'
        })
    }
</script>