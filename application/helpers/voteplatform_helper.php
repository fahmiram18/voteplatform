<?php

function is_logged_in_business(){
    $ci = get_instance();

    if(!$ci->session->userdata('user_id')) {
        redirect('auth/business');
    }
}

function is_logged_in_startup()
{
    $ci = get_instance();

    if (!$ci->session->userdata('startup_id')) {
        redirect('auth/startup');
    }
}

function was_voted()
{
    $ci = get_instance();
    
    if ($ci->session->userdata('startup_vote_remains') == 0){
        redirect('startup/stats');
    }
    
}

function was_voted_business()
{
    $ci = get_instance();

    if ($ci->session->userdata('user_vote_remains') == 0) {
        redirect('business/stats');
    }
}